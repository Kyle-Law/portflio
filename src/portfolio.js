﻿
/* Change this file to get your personal Portfolio */

// Your Summary And Greeting Section

import emoji from "react-easy-emoji";

const greeting = {
  /* Your Summary And Greeting Section */
  username: "Kyle Law",
  title: "Hi, I'm Kyle Law",
  subTitle: emoji("A passionate finance-turned Software Engineer 🚀 experienced building Full Stack Web Applications with Ruby on Rails, React, & JavaScript, leveraging Cloud Services like AWS, Heroku, and Terraform."),
  resumeLink: "https://docs.google.com/document/d/1JsBgMhXp_7HNLE0BXSxbA3X8pQQWqNCqdlk7y_UNjbQ/edit?usp=sharing"
};

// Your Social Media Link

const socialMediaLinks = {

  github: "https://github.com/Kyle-Law",
  linkedin: "https://www.linkedin.com/in/kyle-lawzhunkhing/",
  gmail: "zhunkhing@gmail.com",
  medium: "https://medium.com/@kylelzk",
  // Instagram and Twitter are also supported in the links!
};

// Your Skills Section

const skillsSection = {
  title: "What I do",
  subTitle: "BUILD FULL STACK WEB APP AND ENSURE SITE RELIABILITY",
  skills: [
    emoji("⚡ Develop fast and efficient fully responsive websites"),
    emoji("⚡ Ensure Site Reliability within SLA"),
    emoji("⚡ Integration of third party services such as Sidekiq / Redis / Heroku / AWS / SendGrid / Twilio")
  ],

/* Make Sure You include correct Font Awesome Classname to view your icon
https://fontawesome.com/icons?d=gallery */

  softwareSkills: [
    {
      skillName: "html-5",
      fontAwesomeClassname: "fab fa-html5"
    },
    {
      skillName: "css3",
      fontAwesomeClassname: "fab fa-css3-alt"
    },
    {
      skillName: "sass",
      fontAwesomeClassname: "fab fa-sass"
    },
    {
      skillName: "JavaScript",
      fontAwesomeClassname: "fab fa-js"
    },
    {
      skillName: "reactjs",
      fontAwesomeClassname: "fab fa-react"
    },
    {
      skillName: "ruby on rails",
      fontAwesomeClassname: "fas fa-gem"
    },
    {
      skillName: "npm",
      fontAwesomeClassname: "fab fa-npm"
    },
    {
      skillName: "SQL",
      fontAwesomeClassname: "fas fa-database"
    },
    {
      skillName: "aws",
      fontAwesomeClassname: "fab fa-aws"
    },
    {
      skillName: "azure",
      fontAwesomeClassname: "fab fa-microsoft"
    },
    {
      skillName: "python",
      fontAwesomeClassname: "fab fa-python"
    },
    {
      skillName: "docker",
      fontAwesomeClassname: "fab fa-docker"
    },
    {
      skillName: "D3.js",
      fontAwesomeClassname: "fas fa-chart-bar"
    },
    {
      skillName: "Agile",
      fontAwesomeClassname: "fas fa-shipping-fast"
    },
    {
      skillName: "Kubernetes",
      fontAwesomeClassname: "fas fa-container-storage"
    },
    {
      skillName: "Linux",
      fontAwesomeClassname: "fas fa-linux"
    },
  ]
};

// Your education background

const educationInfo = {
  viewEducation: true, // Set it to true to see education section
  schools: [
    {
      schoolName: "Microverse",
      logo: require("./assets/images/microverseLogo.jpeg"),
      subHeader: "Software Development Program",
      duration: "Jan 2020 - Sep 2020",
      desc: "Completed 30+ projects and published 4 technical articles",
      descBullets: [
        "Developed skills in remote pair-programming, using Github, industry-standard git flows, and daily standups to communicate and collaborate with international remote developers.",
        "Spent 1300+ hours mastering algorithms, data structures, and full-stack development while simultaneously developing projects in Ruby, Rails, Javascript, React, and Redux"
      ]
    },
    {
      schoolName: "Udacity",
      logo: require("./assets/images/udacityLogo.png"),
      subHeader: "Nanodegree Programs",
      duration: "Mar 2020 - Feb 2021",
      desc: "Completed 6 intermediate Udacity Nanodegree Programs",
      descBullets: [
        "AWS Architect Nanodegree",
        "Intermediate JavaScript Nanodegree",
        "Intermediate Python Nanodegree",
        "SQL Nanodegree",
        "Agile Software Developer Nanodegree",
        "Front End Nanodegree",
      ]
    },
    {
      schoolName: "CFA Institute",
      logo: require("./assets/images/cfaInstituteLogo.png"),
      subHeader: "CFA Program",
      duration: "Dec 2018 - June 2019",
      desc: "Passed CFA Level 1 and Level 2 consecutively",
      descBullets: [
        "Scored top 10 percentile among global candidates in CFA Level 1 Dec 2018 exam",
      ]
    },
    {
      schoolName: "Sunway University",
      logo: require("./assets/images/sunwayUniLogo.jpeg"),
      subHeader: "BSc(Hons) Financial Analysis",
      duration: "Aug 2016 - Aug 2019",
      desc: "Represented Sunway University for various external competitions.",
      descBullets: [
        "National Champion of International Quant Championship 2019, represented Malaysia for the Final round held in Singapore in a team of 4",
        "Grand Champion of MFPC Financial Planning Tournament 2018 in a team of 4.",
        "Branding Director of Sunway University Student Council Term 17/18.",
        "2nd Runner Up in MASISWA Selangor Chess Tournament 2018 (Team)",
        "Best Institutional Player of Help University Chess Tournament 2017",
      ]
    }
  ]
}

// Your top 3 proficient stacks/tech experience

const techStack = {
  viewSkillBars: true, //Set it to true to show Proficiency Section
  experience: [
    {
      Stack: "Ruby on Rails & JavaScript",  //Insert stack or technology you have experience in
      progressPercentage: "90%"  //Insert relative proficiency in percentage
    },
    {
      Stack: "AWS Cloud Computing",
      progressPercentage: "80%"
    },
    {
      Stack: "Kubernetes",
      progressPercentage: "80%"
    },
    {
      Stack: "Golang",
      progressPercentage: "60%"
    },
    {
      Stack: "React & Redux",
      progressPercentage: "60%"
    },
    {
      Stack: "Agile Software Development",
      progressPercentage: "90%"
    },
    {
      Stack: "Python",
      progressPercentage: "50%"
    },
    {
      Stack: "SQL",
      progressPercentage: "60%"
    },
    {
      Stack: "React & Redux",
      progressPercentage: "60%"
    },
  ]
};


// Your top 3 work experiences

const workExperiences = {
  viewExperiences: true, //Set it to true to show workExperiences Section
  experience: [
    {
      role: "Independent Contractor",
      company: "Engine Yard | Freelancing",
      companylogo: require("./assets/images/EY_LOGO.png"),
      date: "Aug 2022 – Present",
      desc: "Full Stack Development with Ruby on Rails | Ensure Site Reliability of EngineYard PaaS",
      descBullets: [
        "Troubleshoot Infrastructure or Application-level incidents and minimize downtime for Ruby on Rails applications hosted under Engine Yard",
        "Full Stack development with monolith Rails 7, Hotwire (TurboFrame, TurboStream, StimulusJS), ViewComponent, TailwindCSS, and Minitest.",
        "Provided customer supports to applications hosted on EngineYard PaaS platforms through Zendesk to ensure Site Reliability within Service Level Agreement (SLA).",
        "General software development - performing Rails upgrade from 4 to 5; building Ruby CSV scraper for internal efficiency.",
        "Built Terraform Provider from scratch with Golang for EngineYard Cloud to perform CRUD operations declaratively."
      ]
    },
    {
      role: "DevOps Engineer",
      company: "EasyStore",
      companylogo: require("./assets/images/easystore_logo.png"),
      date: "Aug 2021 – Aug 2022",
      desc: "Building systems to improve business efficiency with Ruby on Rails and JavaScript",
      descBullets: [
        "Import and provision AWS Resources with Infrastructure as Code (IAC) with Terraform with S3 as remote storage, manage Terraform State Drift",
        "Configure Linux servers for staging environments.",
        "Reduce downtime with 80% by setting up Rollback Pipelines.",
        "Convert Rails server from Phusion Passenger to Puma, and perform load-testing with k6.",
        "Configure Sharding with ECS pods and ALB based on HTTP header.",
        "Boost response time by converting frontend-only application to serverless S3 hosting with CloudFront with edge caching.",
        "Improve monitoring by setting up Freshping",
				"Configure EC2 server with dual IP SSH to segregate admins and engineers",
				"Make spot EC2 instances persistent after termination with EIP",
				"Setup serverless email sending with Rails, SQS, and Lambda"
      ]
    },
    {
      role: "Software Engineer",
      company: "HTGroup",
      companylogo: require("./assets/images/HTGroupLogo.png"),
      date: "Oct 2020 – Present",
      desc: "Building systems to improve business efficiency with Ruby on Rails and JavaScript",
      descBullets: [
        "Revamp and prototype new web ordering system, to make tyre ordering process efficient and automated.",
        "Integrated external API such as SendGrid and Twilio to facilitate notification updates for order status",
        "Create Full Stack cross-platform web apps using JavaScript and Ruby on Rails",
        "Leveraged cloud services such as AWS S3 Bucket and Heroku",
        "Built a tracker web app as a substitute for pen and paper car services record",
        "Leveraged knowledge in Full Stack Web Development, Ruby on Rails, JavaScript, Git, and debugged using Chrome Developer Tools",
        "Handling legacy database with SQL"
      ]
    },
    {
      role: "Mentor",
      company: "Microverse",
      companylogo: require("./assets/images/microverseLogo.jpeg"),
      date: "Mar 2020 – Sep 2020",
      desc: "Provided 1-on-1 mentoring sessions to freshers for both technical and professional assistance",
      descBullets: [
        "Provided code review to freshers on GitHub",
        "Practiced professional documentations and Github workflow"
      ]
    }
  ]
};

/* Your Open Source Section to View Your Github Pinned Projects
To know how to get github key look at readme.md */

const openSource = {
  githubConvertedToken: process.env.REACT_APP_GITHUB_TOKEN,
  githubUserName: "Kyle-Law", // Change to your github username to view your profile in Contact Section.
  showGithubProfile :"true" // Set true or false to show Contact profile using Github, defaults to false
};


// Some Big Projects You have worked with your company

// const bigProjects = {
//   title: "Software Projects",
//   subtitle: "SOME STARTUPS AND COMPANIES THAT I HELPED TO CREATE THEIR TECH",
//   projects: [
//     {
//       image: require("./assets/images/saayaHealthLogo.webp"),
//       link: "https://mobil-grab.herokuapp.com/"
//     },
//     {
//       image: require("./assets/images/nextuLogo.webp"),
//       link: "https://wos-prototype-3-s2.herokuapp.com/"
//     }
//   ]
// };

// Your Achievement Section Include Your Certification Talks and More

const achievementSection = {

  title: emoji("Achievements And Certifications 🏆 "),
  subtitle: "Achievements, Certifications, Award Letters and Some Cool Stuff that I have done !",

  achievementsCards: [
    {
      title: "AWS Certified Solutions Architect – Associate",
      subtitle: "Earners of this certification have a comprehensive understanding of AWS services and technologies. They demonstrated the ability to build secure and robust solutions using architectural design principles based on customer requirements. Badge owners are able to strategically design well-architected distributed systems that are scalable, resilient, efficient, and fault-tolerant.",
      image: require("./assets/images/saac02.png"),
      footerLink: [{ name: "Certification", url: "https://www.credly.com/badges/d61b2ac4-2876-4232-b79b-eb6b41b1eac9" }]
    },
    {
      title: "Certified Kubernetes Administrator (CKA)",
      subtitle: "Earners of this designation demonstrated the skills, knowledge and competencies to perform the responsibilities of a Kubernetes Administrator. Earners demonstrated proficiency in Application Lifecycle Management, Installation, Configuration & Validation, Core Concepts, Networking, Scheduling, Security, Cluster Maintenance, Logging / Monitoring, Storage, and Troubleshooting.",
      image: require("./assets/images/CKA.png"),
      footerLink: [{ name: "Certification", url: "https://www.credly.com/badges/05fcdc58-4c5e-464b-8f21-4f5a78bb105b" }]
    },
    // {
    //   title: "Udacity AWS Cloud Architect Nanodegree",
    //   subtitle: "multi-availability architecture, Infrastructure as Code, DevSecOps, 'Shift-Left' security practices, Design for Security",
    //   image: require("./assets/images/udacityLogo.png"),
    //   footerLink: [
    //     { name: "Certification", url: "https://confirm.udacity.com/V7GQDTCN" }
    //   ]
    // },
    // {
    //   title: "Udacity Agile Software Developer Nanodegree",
    //   subtitle: "Agile Manifesto, Frameworks (Scurm, Kanban, XP), Agile Metrics, Scoping, Release and Iteration Planning, MVP, Identifying Risks, Agile Communication",
    //   image: require("./assets/images/udacityLogo.png"),
    //   footerLink: [
    //     { name: "Certification", url: "https://confirm.udacity.com/V7GQDTCN" }
    //   ]
    // },
    {
      title: "HashiCorp Certified: Terraform Associate",
      subtitle: "Cloud Engineering, Infrastructure As Code, Infrastructure Automation, Multi-Cloud Provisioning, Terraform workflow, modules, and configurations.",
      image: require("./assets/images/terraform.png"),
      footerLink: [
        { name: "Certification", url: "https://www.youracclaim.com/badges/5bc0ddb1-bce1-4f8a-85ab-98cdf53cc731/public_url" }
      ]
    },
    // {
    //   title: "Udacity Intermediate Python Nanodegree",
    //   subtitle: "Data Structures, Functional Programming, OOP, File I/O, Modules",
    //   image: require("./assets/images/udacityLogo.png"),
    //   footerLink: [
    //     { name: "Certification", url: "https://confirm.udacity.com/J7G7CUGP" }
    //   ]
    // },
    // {
    //   title: "CFA Level 2 Examination",
    //   subtitle: "Equity Analysis, Quantitative Analysis, Fixed Income, Derivatives, Alternative Investments, Asset Valuation, Financial Reporting and Analysis ",
    //   image: require("./assets/images/cfaInstituteLogo.png"),
    //   footerLink: [{ name: "View Badge", url: "http://basno.com/sk57bo9s" }]
    // },

    // {
    //   title: "Udacity SQL Nanodegree",
    //   subtitle: "SQL Queries, Aggregations, Subqueries, Window Functions, Data Cleaning, Window Functions, Advanced JOINS, Normalizing Data, DDL, DML, Constraints, Performances Tuning, NoSQL - MongoDB & Redis",
    //   image: require("./assets/images/udacityLogo.png"),
    //   footerLink: [
    //     { name: "Certification", url: "https://confirm.udacity.com/6V5DFVEC" }
    //   ]
    // },
    // {
    //   title: "Udacity Frontend Developer Nanodegree",
    //   subtitle: "CSS Flexbox & Grid, JavaScript and the DOM, Web APIs and Asynchronous JS, Build Tools, SPA, Webpack, SASS",
    //   image: require("./assets/images/udacityLogo.png"),
    //   footerLink: [
    //     { name: "Certification", url: "https://confirm.udacity.com/X4MYTVLR" }
    //   ]
    // },
  ]
};

// Blogs Section

const blogSection = {

  title: "Blogs",
  subtitle: "With Love for Developing cool stuff, I love to write and teach others what I have learnt.",

  blogs: [
    {
      url: "https://medium.com/@kylelzk/demystifying-yield-in-ruby-45e6c78ef563",
      title: "Demystifying 'yield' in Ruby",
      description: "Address the common functionalities and applications of 'yield' in Ruby"
    },
    {
      url: "https://medium.com/@kylelzk/how-to-have-a-smoother-code-review-cabb9c04bdd5",
      title: "How to have a smoother code review",
      description: "Few hacks on having a smoother, efficient code review on GitHub"
    },
    {
      url: "https://medium.com/@kylelzk/how-to-fix-common-github-branching-issues-without-any-code-96059f5b820c",
      title: "How to Fix Common github Branching Issues without any code",
      description: "Fixing common issues that may arise during GitHub workflow without any code."
    }
  ]
};

// Talks Sections

// const talkSection = {
//   title: "TALKS",
//   subtitle: emoji("I LOVE TO SHARE MY LIMITED KNOWLEDGE AND GET A SPEAKER BADGE 😅"),

//   talks: [
//     {
//       title: "Build Actions For Google Assistant",
//       subtitle: "Codelab at GDG DevFest Karachi 2019",
//       slides_url: "https://bit.ly/saadpasta-slides",
//       event_url: "https://www.facebook.com/events/2339906106275053/"
//     }
//   ]
// };

// Podcast Section

// const podcastSection = {
//   title: emoji("Podcast 🎙️"),
//   subtitle: "I LOVE TO TALK ABOUT MYSELF AND TECHNOLOGY",

//   // Please Provide with Your Podcast embeded Link
//   podcast: ["https://anchor.fm/codevcast/embed/episodes/DevStory---Saad-Pasta-from-Karachi--Pakistan-e9givv/a-a15itvo"]
// };

const contactInfo = {
  title: emoji("Contact Me ☎️"),
  subtitle: "Discuss a project or just want to say hi? My Inbox is open for all.",
  number: "+6018-3704362",
  email_address: "zhunkhing@gmail.com"
};

//Twitter Section

const twitterDetails = {

  userName : "twitter"//Replace "twitter" with your twitter username without @

};
export { greeting, socialMediaLinks, skillsSection, educationInfo, techStack, workExperiences, openSource, achievementSection, blogSection, contactInfo , twitterDetails};
